//
//  AlertViewController.swift
//  Swap
//
//  Created by Robert Szost on 15/11/2018.
//  Copyright © 2018 Robert Szost. All rights reserved.
//

import UIKit
import UserNotifications

class AlertViewController: UIViewController, UITextFieldDelegate, UNUserNotificationCenterDelegate {
    
    //Variable to storage alert value, picked curency and ititiate notifications and timer
    var pickedCurrency: String!
    var websiteCurrencyValue: Double!
    var timer: Timer!
    var alertValue = ""
    var backgroundTask: UIBackgroundTaskIdentifier = .invalid
    let notification = UNMutableNotificationContent()
    
    //Pre-setup IBOutlets and seague
    @IBOutlet weak var currencySymbol: UILabel!
    @IBOutlet weak var alertValueTextField: UITextField!
    @IBOutlet weak var turnOnOfSwitch: UISwitch!
    
    //Function which evaluate timer to repeat getting currency value from API every 10 sec
    @IBAction func switchTurnOn(_ sender: UISwitch) {
        if turnOnOfSwitch.isOn {
            alertValueTextField.isUserInteractionEnabled = false
            alertValueTextField.textColor = .gray
            timer = Timer.scheduledTimer(timeInterval: 10, target: self,
                                         selector: #selector(backgroundThreadWithCheckingCurrencyValue),
                                         userInfo: nil, repeats: true)
            //Register background task
            registerBackgroundTask()
        }
        else {
            alertValueTextField.isUserInteractionEnabled = true
            alertValueTextField.textColor = .black
            timer?.invalidate()
            timer = nil
        }
    }
    
    //Function which execute after view is loaded into memory
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(registerBackgroundTask),
                                               name: UIApplication.didBecomeActiveNotification, object: nil)
        currencySymbol.text = pickedCurrency
        hideKeyboard()
    }
    
    //Deinitialization for observer created before
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    //Function which create notification if data from API is lower or the same as alarm
    func createNotification(currencyValue: Double){
        if currencyValue <= websiteCurrencyValue{
            let content = UNMutableNotificationContent()
            content.title = "Currency alarm!"
            content.body = "Your currency reached requested value \(currencyValue) PLN"
            content.sound = UNNotificationSound.default
            
            let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
            let request = UNNotificationRequest(identifier: "alarm", content: content, trigger: trigger)
            
            turnOnOfSwitch.setOn(false, animated: true)
            endBackgroundTask()
            UNUserNotificationCenter.current().add(request) { (error) in
                print(error as Any)
            }
        }
    }
    
    //Background task which grabs data from API
    @objc func backgroundThreadWithCheckingCurrencyValue(){
        alertValue = String(alertValueTextField.text!)
        let parseAlertValue = alertValue.replacingOccurrences(of: ",", with: ".")
        ViewController().buildFinalString(pickedCurrency: currencySymbol.text!)
        createNotification(currencyValue: Double(parseAlertValue)!)
    }
    
    //Function which register new background task
    @objc func registerBackgroundTask() {
        backgroundTask = UIApplication.shared.beginBackgroundTask { [weak self] in
            self?.endBackgroundTask()
        }
        assert(backgroundTask != .invalid)
    }
    
    //Function which finish background task and terminate timer
    func endBackgroundTask() {
        print("Background task ended.")
        UIApplication.shared.endBackgroundTask(backgroundTask)
        backgroundTask = .invalid
        timer.invalidate()
    }
    
    //Function which configure notifications
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .sound, .badge])
    }
}


