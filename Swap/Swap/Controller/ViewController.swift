//
//  ViewController.swift
//  Swap
//
//  Created by Robert Szost on 12/11/2018.
//  Copyright © 2018 Robert Szost. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import UserNotifications

class ViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    //Variables to send requests through API
    let baseURL = "https://www.alphavantage.co/query?function=CURRENCY_EXCHANGE_RATE&from_currency=PLN&to_currency="
    let apiKey = "&apikey=STUXQAR111L60N4U"
    let currencyArray = ["AUD", "BRL", "CAD", "CNY", "EUR", "GBP", "HKD", "IDR", "ILS", "INR", "JPY", "MXN", "NOK", "NZD", "PLN", "RON", "RUB", "SEK", "SGD", "USD", "ZAR"]
    var pickedCurrency = ""
    var isMainThread = false
    var mySubstring : Substring = ""
    
    //Pre-setup IBOutlets and seague
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var lastRefreshedLabel: UILabel!
    @IBOutlet weak var currencyPicker: UIPickerView!
    @IBOutlet weak var goToNextScreenButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Setup delegates and datasource for currency picker
        currencyPicker.delegate = self
        currencyPicker.dataSource = self
        
        //Disabled button to set alarm before picking currency
        goToNextScreenButton.isEnabled = false
        isMainThread = true
    }
    
    //Function for initialization number of columns in picker
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    //Function for creating neccessary amount of elements on the picker
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return currencyArray.count
    }
    
    //Function for putting correct titles in picker
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return currencyArray[row]
    }
    
    //Function which grabs which currency was picked evaluate function to build final string
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        pickedCurrency = currencyArray[row]
        buildFinalString(pickedCurrency: pickedCurrency)
    }
    
    //Function which use Alamofire to send request to API and get currency value
    func getCurrencyData(url: String) {
        Alamofire.request(url, method: .get).responseJSON { response in
            if response.result.isSuccess {
//                print("Sucess! Got the currency data")
                let currencyJSON : JSON = JSON(response.result.value!)
                print(currencyJSON)
                self.updateCurrencyData(json: currencyJSON)
            } else {
                print("Error: \(String(describing: response.result.error))")
                self.priceLabel.text = "Connection Problems"                
            }
        }
    }
    
    //Function to build final string for API using picked currency from picker
    func buildFinalString(pickedCurrency: String)  {
        getCurrencyData(url: baseURL + pickedCurrency + apiKey)
    }
    
    //Function parse JSON and input data obtained by getCurrencyData function to the viev
    func updateCurrencyData(json : JSON) {
        let currencyResult = json["Realtime Currency Exchange Rate"]["5. Exchange Rate"].stringValue
        let lastRefreshResult = json["Realtime Currency Exchange Rate"]["6. Last Refreshed"].stringValue
        let index = currencyResult.index(currencyResult.startIndex, offsetBy: 7)
        mySubstring = currencyResult[..<index]
        
        if isMainThread == true {
        priceLabel.text = "\(mySubstring)" + " PLN"
        lastRefreshedLabel.text = "Last update: " +  "\(lastRefreshResult) " + "UTC"
        goToNextScreenButton.isEnabled = true
        }
    }
    
    //Seague to set alarm for picked by user currency
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToAlertScreenSeague" {
            let destinationVC = segue.destination as! AlertViewController
            destinationVC.pickedCurrency = pickedCurrency
            destinationVC.websiteCurrencyValue = Double(mySubstring)
            isMainThread = false
        }
    }
}
